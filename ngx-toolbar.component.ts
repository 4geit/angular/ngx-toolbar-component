import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { MdSnackBar } from '@angular/material';

import { NgxAuthService } from '@4geit/ngx-auth-service';

@Component({
  selector: 'ngx-toolbar',
  template: require('pug-loader!./ngx-toolbar.component.pug')(),
  styleUrls: ['./ngx-toolbar.component.scss']
})
export class NgxToolbarComponent {

  constructor(
    private authService: NgxAuthService,
    private location: Location,
    private snackBar: MdSnackBar
  ) { }

  isPageActive(page: any) {
    if (typeof page === 'string') {
      return page === this.location.path();
    }
    for (let i = 0; i < page.length; i++) {
      if (page[i] === this.location.path()) {
        return true;
      }
    }
    return false;
  }

}
