# @4geit/ngx-toolbar-component [![npm version](//badge.fury.io/js/@4geit%2Fngx-toolbar-component.svg)](//badge.fury.io/js/@4geit%2Fngx-toolbar-component)

---

add a toolbar using MdToolbar and provide a set of parameters to configure

## Installation

1. A recommended way to install ***@4geit/ngx-toolbar-component*** is through [npm](//www.npmjs.com/search?q=@4geit/ngx-toolbar-component) package manager using the following command:

```bash
npm i @4geit/ngx-toolbar-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/ngx-toolbar-component
```

2. You need to import the `NgxToolbarComponent` component within the module you want it to be. For instance `app.module.ts` as follows:

```js
import { NgxToolbarComponent } from '@4geit/ngx-toolbar-component';
```

And you also need to add the `NgxToolbarComponent` component with the `@NgModule` decorator as part of the `declarations` list.

```js
@NgModule({
  // ...
  declarations: [
    // ...
    NgxToolbarComponent,
    // ...
  ],
  // ...
})
export class AppModule { }
```

3. You can also attach the component to a route in your routing setup. To do so, you need to import the `NgxToolbarComponent` component in the routing file you want to use it. For instance `app-routing.module.ts` as follows:

```js
import { NgxToolbarComponent } from '@4geit/ngx-toolbar-component';
```

And you also need to add the `NgxToolbarComponent` component within the list of `routes` as follows:

```js
const routes: Routes = [
  // ...
  { path: '**', component: NgxToolbarComponent }
  // ...
];
```
